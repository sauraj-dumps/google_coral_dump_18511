## coral-user 12 SP1A.211105.002 7743617 release-keys
- Manufacturer: google
- Platform: msmnile
- Codename: coral
- Brand: google
- Flavor: coral-user
- Release Version: 12
- Id: SP1A.211105.002
- Incremental: 7743617
- Tags: release-keys
- CPU Abilist: arm64-v8a,armeabi-v7a,armeabi
- A/B Device: true
- Locale: en-US
- Screen Density: undefined
- Fingerprint: google/coral/coral:12/SP1A.211105.002/7743617:user/release-keys
- OTA version: 
- Branch: coral-user-12-SP1A.211105.002-7743617-release-keys
- Repo: google_coral_dump_18511


>Dumped by [Phoenix Firmware Dumper](https://github.com/DroidDumps/phoenix_firmware_dumper)
